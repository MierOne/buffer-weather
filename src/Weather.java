import java.io.*;
import java.util.*;

public class Weather {
      public static void main(String[] args) {
            try {
                  double sumOfTemperatures = 0;
                  double sumOfHumidity = 0;
                  double sumOfWindSpeed = 0;
                  double maxTemp = Double.MIN_VALUE;
                  double minHumidity = Double.MAX_VALUE;
                  double maxWindSp = Double.MIN_VALUE;

                  int lineWithMaxTemp = -1;
                  int lineWithMinHum = -1;
                  int lineWithMaxWS = -1;
                  ArrayList<String> timestampsTemps = new ArrayList<>();
                  ArrayList<String> timestampsHum = new ArrayList<>();
                  ArrayList<String> timestampsWS = new ArrayList<>();

                  int[] windDirectionFrequency = new int[4];

                  FileReader fileReader = new FileReader("src/dataexport_20210320T064822.csv");
                  BufferedReader bufferedReader = new BufferedReader(fileReader);

                  for (int i = 0; i < 10; i++) {
                        bufferedReader.readLine();
                  }

                  for (int i = 0; i < 192; i++) {
                        String[] colums = bufferedReader.readLine().split(",");
                        sumOfTemperatures += Double.parseDouble(colums[1]);
                        sumOfHumidity += Double.parseDouble(colums[2]);
                        sumOfWindSpeed += Double.parseDouble(colums[3]);

                        if (Double.parseDouble(colums[1]) > maxTemp) {
                              maxTemp = Double.parseDouble(colums[1]);
                              timestampsTemps.add(colums[0]);
                              lineWithMaxTemp++;
                        }
                        if (Double.parseDouble(colums[2]) < minHumidity) {
                              minHumidity = Double.parseDouble(colums[2]);
                              timestampsHum.add(colums[0]);
                              lineWithMinHum++;
                        }
                        if (Double.parseDouble(colums[3]) > maxWindSp) {
                              maxWindSp = Double.parseDouble(colums[3]);
                              timestampsWS.add(colums[0]);
                              lineWithMaxWS++;
                        }
                        if (Double.parseDouble(colums[4]) > 315 || Double.parseDouble(colums[4]) <= 45) {
                              windDirectionFrequency[0]++;
                        }
                        if (Double.parseDouble(colums[4]) > 45 || Double.parseDouble(colums[4]) <= 135) {
                              windDirectionFrequency[1]++;
                        }
                        if (Double.parseDouble(colums[4]) > 135 || Double.parseDouble(colums[4]) <= 225) {
                              windDirectionFrequency[2]++;
                        }
                        if (Double.parseDouble(colums[4]) > 225 || Double.parseDouble(colums[4]) <= 315) {
                              windDirectionFrequency[3]++;
                        }
                  }
                  bufferedReader.close();

                  FileWriter fileWriter = new FileWriter("src/Needed Weather Data.txt");
                  BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

                  bufferedWriter.write("Средняя температура: " + (sumOfTemperatures/(192)) + "\n");
                  bufferedWriter.write("Среднее значение влажности: " + (sumOfHumidity/192) + "\n");
                  bufferedWriter.write("Средняя скорость ветра: " + (sumOfWindSpeed/192) + "\n");

                  bufferedWriter.write("Время и день когда была максимальная температура: " + timeOutput(timestampsTemps, lineWithMaxTemp) + "\n");
                  bufferedWriter.write("Время и день когда была минимальная влажность: " + timeOutput(timestampsHum, lineWithMinHum) + "\n");
                  bufferedWriter.write("Время и день когда была максимальная скорость ветра: " + timeOutput(timestampsWS, lineWithMaxWS) + "\n");

                  int max = windDirectionFrequency[0];
                  int index = 0;
                  for (int i = 1; i < 4; i++) {
                        if(windDirectionFrequency[i]<max){
                              max = windDirectionFrequency[i];
                              index = i;
                        }
                  }
                  switch (index){
                        case(0):
                              bufferedWriter.write("Наиболее частое направление ветра: Север");
                              break;
                        case(1):
                              bufferedWriter.write("Наиболее частое направление ветра: Восток");
                              break;
                        case(2):
                              bufferedWriter.write("Наиболее частое направление ветра: Юг");
                              break;
                        case(3):
                              bufferedWriter.write("Наиболее частое направление ветра: Запад");
                              break;
                  }
                  bufferedWriter.close();
            } catch (IOException e) {
                  e.printStackTrace();
            }
      }

      public static String timeOutput(ArrayList<String> strings, int line) {
            String str = strings.get(line);
            return "День/Месяц/Год " + str.charAt(6) + str.charAt(7) + "/" + str.charAt(4) + str.charAt(5) + "/" + str.charAt(0) + str.charAt(1) +
                    str.charAt(2) + str.charAt(3) + ". Время " + str.charAt(9) + str.charAt(10) + " часов " + str.charAt(11) + str.charAt(12) + " минут.";
      }
}